#!/bin/bash
#PJM -L "rscunit=rscunit_ft02"
#PJM -L "rscgrp=def_grp"
#PJM -L "elapse=01:00:00"
#PJM -L "node=1"
#PJM -N "m_1_D3_hg38_k1_sve512_fcc-comp3"

LANG_HOME=/opt/FJSVxtclanga/tcsds-1.2.26
export PATH=${LANG_HOME}/bin:${PATH}
export LD_LIBRARY_PATH=${LANG_HOME}/lib64

#export XOS_MMM_L_HPAGE_TYPE=none
#export XOS_MMM_L_COLORING=0
export XOS_MMM_L_PAGING_POLICY=demand:demand:demand
export XOS_MMM_L_ARENA_LOCK_TYPE=0
export XOS_MMM_L_HUGETLB_FAILSAFE=0

perf record -e cache-misses -- ./bwa-mem2/bwa-mem2-fcc-comp3 mem -t 1 -1 -K 1000000 -o outputs/output_D3hg381k1sve512fcc-comp3.sa inputs-comp3/hg38.fa inputs/D3.fasta
