import subprocess
import time

def numa_conf(threads):
    if threads > 36:
        return "4,5,6,7"
    elif threads > 24:
        return "4,5,6"
    elif threads > 12:
        return "4,5"
    else:
        return "4"

def execute():
    for r,i in ins:
        for t,km in ts:
            n=numa_conf(int(t))
            for vl in vls:
                k = km + "000000"
                s = template.format(t=t,r=r,i=i,n=n,k=k,km=km,vl=vl,comp=comp)
                f = open("scripts/sub.sh","w")
                f.write(s)
                f.close()
                p = subprocess.Popen(["pjsub","scripts/sub.sh"],stdout=subprocess.PIPE)
                out,err = p.communicate()
                if out != None:
                    print out,
                if err != None:
                    print err,

template = """#!/bin/bash
#PJM -L "rscunit=rscunit_ft02"
#PJM -L "rscgrp=def_grp"
#PJM -L "elapse=24:00:00"
#PJM -L "node=1"
###PJM --mpi "proc=48,max-proc-per-node=48"
#PJM -N "m_{t}_{i}_{r}_k{km}_sve{vl}_{comp}"

LANG_HOME=/opt/FJSVxtclanga/tcsds-1.2.26
export PATH=${{LANG_HOME}}/bin:${{PATH}}
export LD_LIBRARY_PATH=${{LANG_HOME}}/lib64

#export XOS_MMM_L_HPAGE_TYPE=none
#export XOS_MMM_L_COLORING=0
export XOS_MMM_L_PAGING_POLICY=demand:demand:demand
export XOS_MMM_L_ARENA_LOCK_TYPE=0
export XOS_MMM_L_HUGETLB_FAILSAFE=0

    #time numactl --cpunodebind={n} --interleave={n} -- ./setvl {vl} ./bwa-mem2/bin/bwa-mem2-{comp} mem -t {t} -1 -K {k} -o outputs/output_{i}{r}{t}k{km}sve{vl}{comp}.sa inputs-comp3/{r}.fa inputs/{i}.fasta
    #time ./setvl {vl} ./bwa-mem2/bin/bwa-mem2-{comp} mem -t {t} -1 -K {k} -o outputs/output_{i}{r}{t}k{km}sve{vl}{comp}.sa inputs-comp3/{r}.fa inputs/{i}.fasta

    cp inputs/{i}.fasta /dev/shm/
    time ./setvl {vl} ./bwa-mem2/bin/bwa-mem2-{comp} mem -t {t} -1 -K {k} -o /dev/null inputs-comp3/{r}.fa /dev/shm/{i}.fasta
"""

#ins=[("ffg","F1"),("ffg","F2"),("ffg","F3")]
ins=[("hg38","D3"),("hg38","D4"),("hg38","D5")]
#ts=[("1","10"),("12","120"),("24","240"),("48","480")] # threads,k(M)
#vls=["128","256","512"]
#versions = "base lp inline popcnt nseqs2 nseqs4 nseqs8 pred".split()

# OPTIMIZATIONS
ts=[("48","480")] # threads,k(M)
vls=["512"]
versions = "base lp inline popcnt nseqs pred".split()
for v in versions:
    comp=v
    execute()

# SVE ANALYSIS AND SKX COMPARISON
#ts=[("1","10"),("12","120"),("24","240"),("48","480")] # threads,k(M)
#vls=["128","256","512"]
#versions = "base pred".split()
#comp=""
#for v in versions:
#    comp=v
#    execute()

