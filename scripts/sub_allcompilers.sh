#!/bin/bash
#PJM -L "rscunit=rscunit_ft02"
#PJM -L "rscgrp=def_grp"
#PJM -L "elapse=24:00:00"
#PJM -L "node=1"
#PJM -N "m_allcompilers"

LANG_HOME=/opt/FJSVxtclanga/tcsds-1.2.26
export PATH=${LANG_HOME}/bin:${PATH}
export LD_LIBRARY_PATH=${LANG_HOME}/lib64

#export XOS_MMM_L_HPAGE_TYPE=none
#export XOS_MMM_L_COLORING=0
export XOS_MMM_L_PAGING_POLICY=demand:demand:demand
export XOS_MMM_L_ARENA_LOCK_TYPE=0
export XOS_MMM_L_HUGETLB_FAILSAFE=0

for input in D3 D4 D5
do
    echo ${input}
    echo "No affinity"
    for i in {1..10}
    do
        ./bwa-mem2/bin/bwa-mem2-noaff mem -t 48 -1 -K 480000000 -o /dev/null inputs-comp3/hg38.fa inputs/${input}.fasta
    done

    echo "Affinity"
    for i in {1..10}
    do
        ./bwa-mem2/bin/bwa-mem2-aff mem -t 48 -1 -K 480000000 -o /dev/null inputs-comp3/hg38.fa inputs/${input}.fasta
    done

    echo "GCC"
    ./bwa-mem2/bin_gcc/bwa-mem2-base mem -t 48 -1 -K 480000000 -o /dev/null inputs-comp3/hg38.fa inputs/${input}.fasta

    echo "ARMCLANG"
    ./bwa-mem2/bin_armclang/bwa-mem2-base mem -t 48 -1 -K 480000000 -o /dev/null inputs-comp3/hg38.fa inputs/${input}.fasta
done

