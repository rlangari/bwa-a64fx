module load gcc/11.1
rm -f bwa-mem2-* compile_gcc.sh.*

make cleanall
make EXE="bwa-mem2-base" CXX="g++ -DBACK_INLINE=0 -DPOP_CNT=0 -DNSEQS=1 -DBSW_PREDICATION=0" CC="gcc" COMP="-DSA_COMPX=3"

make cleanall
make EXE="bwa-mem2-inline" CXX="g++ -DBACK_INLINE=1 -DPOP_CNT=0 -DNSEQS=1 -DBSW_PREDICATION=0" CC="gcc" COMP="-DSA_COMPX=3"

make cleanall
make EXE="bwa-mem2-popcnt" CXX="g++ -DBACK_INLINE=1 -DPOP_CNT=1 -DNSEQS=1 -DBSW_PREDICATION=0" CC="gcc" COMP="-DSA_COMPX=3"

make cleanall
make EXE="bwa-mem2-nseqs" CXX="g++ -DBACK_INLINE=1 -DPOP_CNT=1 -DNSEQS=4 -DBSW_PREDICATION=0" CC="gcc" COMP="-DSA_COMPX=3"

make cleanall
make EXE="bwa-mem2-pred" CXX="g++ -DBACK_INLINE=1 -DPOP_CNT=1 -DNSEQS=4 -DBSW_PREDICATION=1" CC="gcc" COMP="-DSA_COMPX=3"

mv bwa-mem2-* bin_gcc

