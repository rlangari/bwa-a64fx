module load fuji
rm -f bwa-mem2-* compile_gcc.sh.*

make cleanall
make EXE="bwa-mem2-base" CXX="fccpx -Nclang -lstdc++ -ffj-no-largepage -DBACK_INLINE=0 -DPOP_CNT=0 -DNSEQS=1 -DBSW_PREDICATION=0" CC="fccpx" COMP="-DSA_COMPX=3"

make cleanall
make EXE="bwa-mem2-lp" CXX="fccpx -Nclang -lstdc++ -DBACK_INLINE=0 -DPOP_CNT=0 -DNSEQS=1 -DBSW_PREDICATION=0" CC="fccpx" COMP="-DSA_COMPX=3"

make cleanall
make EXE="bwa-mem2-inline" CXX="fccpx -Nclang -lstdc++ -DBACK_INLINE=1 -DPOP_CNT=0 -DNSEQS=1 -DBSW_PREDICATION=0" CC="fccpx" COMP="-DSA_COMPX=3"

make cleanall
make EXE="bwa-mem2-popcnt" CXX="fccpx -Nclang -lstdc++ -DBACK_INLINE=1 -DPOP_CNT=1 -DNSEQS=1 -DBSW_PREDICATION=0" CC="fccpx" COMP="-DSA_COMPX=3"

make cleanall
make EXE="bwa-mem2-nseqs" CXX="fccpx -Nclang -lstdc++ -DBACK_INLINE=1 -DPOP_CNT=1 -DNSEQS=4 -DBSW_PREDICATION=0" CC="fccpx" COMP="-DSA_COMPX=3"

make cleanall
make EXE="bwa-mem2-pred" CXX="fccpx -Nclang -lstdc++ -DBACK_INLINE=1 -DPOP_CNT=1 -DNSEQS=4 -DBSW_PREDICATION=1" CC="fccpx" COMP="-DSA_COMPX=3"

mv bwa-mem2-* bin

