import sys
import os
import re
from parse_times import get_params

path = "."

if len(sys.argv) > 1:
    path = sys.argv[1]

def parse():
    d={}

    for vl in vls:
        for k in ks:
            for comp in comps:
                for inp in inputs:
                    key = c+"_"+inp+"_sve"+vl+"_k"+k+"_"+comp
                    for s in phases:
                        d[key+"_"+s]={}
                    for filename in os.listdir(path):
                        if re.match("m_"+c+"_"+inp+".*_k"+k+"_sve"+vl+"_"+comp+".*out",filename):
                            with open(path+"/"+filename,"r") as f:
                                for line in f:
                                    if ":" not in line:
                                        continue
                                    s = line.split(":")
                                    name = s[0].strip()
                                    num = s[1].strip()
                                    for s in phases:
                                        if name not in d[key+"_"+s]:
                                            d[key+"_"+s][name] = num
                                            break

    for key in sorted(d.itervalues().next()):
        print ","+key,
    print ""

    for vl in vls:
        for k in ks:
            for comp in comps:
                for inp in inputs:
                    for s in phases:
                        key = c+"_"+inp+"_sve"+vl+"_k"+k+"_"+comp+"_"+s
                        print key,
                        for key2,value in sorted(d[key].iteritems()):
                            print ","+value,
                        print ""

phases=["SMEM","SAL","BSW"]
inputs=["D3","D4","D5"]
vls,comps = get_params(path)
comps = "base lp inline popcnt nseqs pred".split()

print("hg38")
#c="1"
#ks=["10"]
#parse()
#c="12"
#ks=["120"]
#parse()
#c="24"
#ks=["240"]
#parse()
c="48"
ks=["480"]
parse()

