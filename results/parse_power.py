import sys
import os
import re

path = "."

if len(sys.argv) > 1:
    path = sys.argv[1]

def get_params():
    comps = []
    vls = []
    for filename in os.listdir(path):
        if re.match("m_.*",filename):
            comp = filename.split("_")[6].split(".")[0]
            if comp not in comps:
                comps.append(comp)
            vl = filename.split("_")[5][3:]
            if vl not in vls:
                vls.append(vl)
    vls.sort(key=int)
    return vls,comps

def parse():
    for vl in vls:
        for k in ks:
            for comp in comps:
                print(c + "_" + vl),
                for inp in inputs:
                    for filename in os.listdir(path):
                        if re.match("m_"+c+"_"+inp+".*_k"+k+"_sve"+vl+"_"+comp+".*out",filename):
                            with open(path+"/"+filename,"r") as f:
                                for line in f:
                                    if line.startswith("joules "):
                                        s = line.split(" ")[1].strip()
                                        print(","+s),
                    for filename in os.listdir(path):
                        if re.match("m_"+c+"_"+inp+".*_k"+k+"_sve"+vl+"_"+comp+".*err",filename):
                            with open(path+"/"+filename,"r") as f:
                                for line in f:
                                    if "PROCESS() (Total compute time + (read + SAM) IO time)" in line:
                                        s = line.split(":")[1].strip()
                                        print(","+s),
                print("")

def parse_times():
    for vl in vls:
        for k in ks:
            for comp in comps:
                print(c + "_" + vl + "_k" + k + "_" + comp),
                for inp in inputs:
                    for filename in os.listdir(path):
                        if re.match("m_"+c+"_"+inp+".*_k"+k+"_sve"+vl+"_"+comp+".*err",filename):
                            with open(path+"/"+filename,"r") as f:
                                for line in f:
                                    if line.startswith("real"):
                                        s = line.split("\t")[1][:-2].strip()
                                        minutes = int(s.split("m")[0])
                                        segs = int(s.split("m")[1].split(".")[0])
                                        total = minutes * 60 + segs
                                        print(","+str(total)),
                print("")


def parse_times2():
    for vl in vls:
        for k in ks:
            for comp in comps:
                print(c + "_" + vl + "_k" + k + "_" + comp),
                for inp in inputs:
                    for filename in os.listdir(path):
                        if re.match("m_"+c+"_"+inp+".*_k"+k+"_sve"+vl+"_"+comp+".*err",filename):
                            with open(path+"/"+filename,"r") as f:
                                for line in f:
                                    if "PROCESS() (Total compute time + (read + SAM) IO time)" in line:
                                        s = line.split(":")[1].strip()
                                        print(","+s),
                print("")
def parse_joules():
    for vl in vls:
        for k in ks:
            for comp in comps:
                print(c + "_" + vl + "_k" + k + "_" + comp),
                for inp in inputs:
                    for filename in os.listdir(path):
                        if re.match("m_"+c+"_"+inp+".*_k"+k+"_sve"+vl+"_"+comp+".*out",filename):
                            with open(path+"/"+filename,"r") as f:
                                for line in f:
                                    if line.startswith("joules "):
                                        s = line.split(" ")[1].strip()
                                        print(","+s),
                print("")

inputs = ["D3","D4","D5"]
vls,comps = get_params()

if __name__=="__main__":
    c="1"
    ks=["10"]
    parse()
    c="12"
    ks=["120"]
    parse()
    c="24"
    ks=["240"]
    parse()
    c="48"
    ks=["480"]
    parse()

