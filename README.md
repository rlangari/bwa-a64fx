# BWA-MEM2 ported and optimized for the A64FX processor

## Introduction

BWA-MEM2 (https://github.com/bwa-mem2/bwa-mem2) is an alignment tool used for mapping DNA sequences against a large reference genome, such as the human genome.
In this project, we port BWA-MEM2 to the AArch64 architecture using the ARMv8-A specification.
The porting effort entails numerous code modifications, since BWA-MEM2 implements certain kernels using x86\_64 specific intrinsics, e.g., AVX-512.
To adapt this code we use the recently introduced Arm's Scalable Vector Extensions (SVE).
More specifically, we use Fujitsu's A64FX processor, the first to implement SVE.
The A64FX powers the Fugaku Supercomputer that led the Top500 ranking during 2020, and 2021.
After porting BWA-MEM2 we define and implement a number of optimizations to improve performance in the A64FX target architecture.

This software is distributed under the GPLv3 license, and it runs on the command line under Linux.

## Requirements

- **A64FX:** all this work has been developed using the A64FX processor. We enourage the developers to try the code on other AArch64+SVE systems. However, we do not know how it works on them.
- **Inputs sequences:** for the evaluation we use 3 input sequences, D3, D4 and D5, available at: https://gitlab.bsc.es/rlangari/bwa-mem2-input-sequences. This input sequences were taken from: https://www.ncbi.nlm.nih.gov/sra/SRX020470, https://www.ncbi.nlm.nih.gov/sra/SRX207170 and https://www.ncbi.nlm.nih.gov/sra/SRX206890
- **Human genome:** we use the human genome as reference, available at: http://hgdownload.cse.ucsc.edu/goldenPath/hg38/bigZips/

## Usage

For the inputs, **create two directories**: `inputs` and `inputs-comp3` for the inputs sequences, and for the human genome respectively.

Compile `setvl.c` file with the command `gcc setvl.c -o setvl`

In order to **compile**, go to `bwa-mem2` directory and execute `compile.sh` for Fujitsu C Compiler (fcc), `compile_gcc.sh` for GNU C Compiler (gcc), or `compile_armclang.sh` for Arm Clang Compiler (armclang).
This scripts leaves the binary files in `bin`, `bin_gcc` and `bin_armclang` respectively.

In order to **create the index**, go to the root directory, change the binary path inside the file `scripts/build_index.sh` and execute `./scripts/build_index.sh`.

To **execute**, go to the `scripts` directory and configure the `sub.py` script in order to match your preferences, such as binaries directory, number of threads, code version, inputs...
Then, go back to the root directory, and from there execute `python scripts/sub.py`.
This will launch the template with all the configurations you selected.

The final results can be found in the root directory with the prefix `m_`.

<!--
## Citation

If you use this work for your published research, please cite the following paper:

xxx
-->
