#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/prctl.h>

int main (int argc, char *argv[], char *envp[]) {
    if (argc < 3) {
        fprintf(stderr, "Usage: %s <vector_length> <command>\n",argv[0]);
        return 1;
    }

    char *tmp;
    long vl = strtol(argv[1], &tmp, 10);
    if (vl < 128 || vl % 128 != 0 || vl > 2048) {
        fprintf(stderr, "Invalid vector length: %ld\n", vl);
        return 1;
    }

    vl = vl / 8; // Convert to bytes

    if (prctl(PR_SVE_SET_VL, vl | PR_SVE_SET_VL_ONEXEC) == -1) {
        perror("Error changing the vector length\n");
        return 1;
    }

    execve(argv[2], argv + 2, envp);

    // Program should not reach this point
    return 1;
}

